module Convert.IntegerInt (
    integer2int,
    int2integer
    ) where

integer2int :: Integer -> Int
integer2int = fromInteger

int2integer :: Int -> Integer
int2integer = toInteger
