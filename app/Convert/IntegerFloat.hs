module Convert.IntegerFloat (
    integer2float,
    float2integer
    ) where

integer2float :: Integer -> Float
integer2float = fromInteger

float2integer :: Float -> Integer
float2integer = floor
