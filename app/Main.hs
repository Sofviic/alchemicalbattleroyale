module Main (main) where

import Display.Gloss.Main
import Game.WorldGeneration


main :: IO ()
main = startGame --putStrLn $ displayWorldAsText 10 initWorld
