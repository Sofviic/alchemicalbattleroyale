module Game.Types where

import Game.Colour
import Coordinates (Coordinates)
import qualified Coordinates as Coord

type World = [Tile]

data Tile = Tile {
    position :: Coordinates,
    colour :: Colour,
    sprite :: Char
} deriving (Show,Eq)
