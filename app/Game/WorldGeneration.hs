module Game.WorldGeneration where

import Coordinates (Coordinates)
import Game.Colour
import Game.Types
import Game.Tiles

worldRadius :: Integer
worldRadius = 10

worldCoordinates :: [Coordinates]
worldCoordinates = [(x,y) | 
                    x <- [(-worldRadius)..worldRadius], 
                    y <- [(-worldRadius)..worldRadius]
                    ]

initWorld :: World
initWorld = (playerTile (0, 0) :) . fmap grassTile . filter (/= (0,0)) $ worldCoordinates


    