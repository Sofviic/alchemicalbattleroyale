module Game.Tiles where
    
import Game.Colour
import Coordinates (Coordinates)
import Game.Types

grassTile :: Coordinates -> Tile
grassTile c = Tile{position=c, colour=cGreen, sprite='.'}

playerTile :: Coordinates -> Tile
playerTile c = Tile{position=c, colour=cBlue, sprite='@'}

