module Game.Colour where

import Convert.IntegerInt

data Colour = Colour {
    red :: Integer,
    green :: Integer,
    blue :: Integer,
    alpha :: Integer
}

instance Show Colour where
    show colour = "#" ++ hexify (red colour) ++ hexify (green colour) ++ hexify (blue colour) ++ hexify (alpha colour)
        where
            hexify :: Integer -> String
            hexify n = [hexifyDigit ((n `div` 16) `mod` 16)] ++ [hexifyDigit (n `mod` 16)]
            hexifyDigit :: Integer -> Char
            hexifyDigit = ("0123456789abcdef" !!) . integer2int

instance Eq Colour where
    c1 == c2 = all id [
        red c1 == red c2,
        green c1 == green c2,
        blue c1 == blue c2,
        alpha c1 == alpha c2
        ]

greyColour :: Integer -> Colour
greyColour x = Colour {red = x, green = x, blue = x, alpha = 255}

greyColourA :: Integer -> Integer -> Colour
greyColourA a x = Colour {red = x, green = x, blue = x, alpha = a}

-- TODO: change to `sum . zipWith (*) [.2126, .7152, .0722] [r,g,b]`
greyscale :: Colour -> Colour
greyscale Colour{red=r,green=g,blue=b,alpha=a} = greyColourA a $ (r + g + b) `div` 3

cRed,cGreen,cBlue,cBlack,cDarkgrey,cLightgrey,cWhite,cTransparent :: Colour
cRed     = Colour {red = 255, green = 0, blue = 0, alpha = 255}
cGreen   = Colour {red = 0, green = 255, blue = 0, alpha = 255}
cBlue    = Colour {red = 0, green = 0, blue = 255, alpha = 255}
cBlack       = greyColourA 255 0
cDarkgrey    = greyColourA 255 120
cLightgrey   = greyColourA 255 200
cWhite       = greyColourA 255 255
cTransparent = greyColourA 0 0


