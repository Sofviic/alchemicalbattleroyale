module Display.Gloss.Tiles where

import Convert.IntegerInt
import Convert.IntegerFloat
import qualified Graphics.Gloss as G
import Game.Colour
import Game.Types

tileSize :: Float
tileSize = 10

tileBorder :: Float
tileBorder = 2

displayWorld :: World -> G.Picture
displayWorld = G.pictures . fmap displayTile

displayTile :: Tile -> G.Picture
displayTile t@Tile{position=(x,y)} = G.translate 
                                        (integer2float x * tileSize) 
                                        (integer2float y * tileSize)
                                        . G.pictures
                                        . fmap ($ t)
                                        $ [displayTileOuter, displayTileInner]

displayTileOuter :: Tile -> G.Picture
displayTileOuter Tile{colour=c,sprite=chr} =
                        G.color G.black
                        $ G.rectangleSolid tileSize tileSize


displayTileInner :: Tile -> G.Picture
displayTileInner Tile{colour=c,sprite=chr} =
                        G.color (convertColour c)
                        $ G.rectangleSolid (tileSize-tileBorder) (tileSize-tileBorder)

convertColour :: Colour -> G.Color
convertColour Colour{red=r,green=g,blue=b,alpha=a} = G.makeColorI ri gi bi ai
                                                where
                                                    ri = integer2int r
                                                    gi = integer2int g
                                                    bi = integer2int b
                                                    ai = integer2int a
