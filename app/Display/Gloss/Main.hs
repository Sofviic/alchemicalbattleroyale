module Display.Gloss.Main (bgColour,fps,startGame) where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Interact
import Game.Types
import Game.WorldGeneration
import Display.Gloss.Tiles

displayModeFullScreen,displayModeWindowed :: Display
displayModeFullScreen = FullScreen
displayModeWindowed = InWindow "ABRoyale" (1920,1080) (960,540)

bgColour :: Color
bgColour = makeColorI 120 0 0 255

fps :: Int
fps = 60

displayFrame :: World -> Picture
displayFrame = displayWorld

io :: Event -> World -> World
io _ = id

update :: Float -> World -> World
update _ = id

startGame :: IO ()
startGame = play displayModeWindowed bgColour fps initWorld displayFrame io update
