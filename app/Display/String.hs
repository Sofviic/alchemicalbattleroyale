module Display.String where

import Game.Colour
import Game.WorldGeneration
import Game.Types
import Game.Tiles
import Data.List

displayWorldAsText :: Integer -> World -> String
displayWorldAsText r w = unlines 
                        . fmap (flip displayWorldAsTextLine w) 
                        . reverse 
                        $ [(-r)..r]

displayWorldAsTextLine :: Integer -> World -> String
displayWorldAsTextLine y = hackcolourString
    . fmap (\t -> (colour t, [sprite t]))
    . sortOn (fst . position)
    . filter ((==y) . snd . position)

colourHack :: Colour -> String
colourHack c | c == cBlack    = "\ESC[30m"
colourHack c | c == cRed      = "\ESC[31m"
colourHack c | c == cGreen    = "\ESC[32m"
colourHack c | c == cBlue     = "\ESC[34m"
colourHack c | c == cWhite    = "\ESC[37m"
colourHack c | otherwise      = uncolourHack

uncolourHack :: String
uncolourHack = "\ESC[0m"

hackcolourString :: [(Colour,String)] -> String
hackcolourString [] = uncolourHack
hackcolourString ((c,s):xs) = colourHack c ++ s ++ hackcolourString xs
{--
default = "\ESC[0m"
colourHack cBlack    = "\ESC[30m"
colourHack cRed      = "\ESC[31m"
colourHack cGreen    = "\ESC[32m"
colourHack cYellow   = "\ESC[33m"
colourHack cBlue     = "\ESC[34m"
colourHack cMagenta  = "\ESC[35m"
colourHack cCyan     = "\ESC[36m"
colourHack cWhite    = "\ESC[37m"
colourHack cBlack    = "\ESC[90m"
colourHack cRed      = "\ESC[91m"
colourHack cGreen    = "\ESC[92m"
colourHack cYellow   = "\ESC[93m"
colourHack cBlue     = "\ESC[94m"
colourHack cMagenta  = "\ESC[95m"
colourHack cCyan     = "\ESC[96m"
colourHack cWhite    = "\ESC[97m"
--}
