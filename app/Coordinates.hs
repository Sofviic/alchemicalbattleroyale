module Coordinates where

type Dim = Integer
type Coordinates = (Dim,Dim)

-- Note to my future self: Try to have all functions output (Coordinates -> Coordinates)

bimap :: (Dim -> Dim) -> (Dim -> Dim) -> Coordinates -> Coordinates
bimap f g (x,y) = (f x, g y)

shift :: Dim -> Dim -> Coordinates -> Coordinates
shift x y = bimap (+x) (+y)

scale :: Dim -> Coordinates -> Coordinates
scale k = bimap (k*) (k*)

add :: Coordinates -> Coordinates -> Coordinates
add = uncurry shift

sub :: Coordinates -> Coordinates -> Coordinates
sub = uncurry shift . scale (-1)

infixr 7 *^
(*^) :: Dim -> Coordinates -> Coordinates
(*^) = scale

infixl 7 ^*
(^*) :: Coordinates -> Dim -> Coordinates
(^*) = flip scale

infixl 5 ^+^
(^+^) :: Coordinates -> Coordinates -> Coordinates
(^+^) = add

up,down,left,right :: Coordinates -> Coordinates
up = shift 0 1
down = shift 0 (-1)
left = shift (-1) 0
right = shift 1 0
